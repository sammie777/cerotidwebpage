package LinerAutomationExample;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {

	public static void main(String[] args) {

		// Setting the system path to utlize the chrome drive


		// Creating Webdriver object to drive my browser

		WebDriver driver = new ChromeDriver();

		driver.get("http://www.google.com");
		// maximize the browser
		driver.manage().window().maximize();
		
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		txtBoxSearch.sendKeys("What is selenium");

		WebElement searchBtn = driver.findElement(By.xpath("//input[@name='btnK']"));

		searchBtn.sendKeys(Keys.RETURN);

	}

}
