package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidWebsitePageElements;

public class CerotidWebsitePageElementTest {
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {

		invokeBrowserCerotidPage1();
		fillform();
		

	}

	public static void invokeBrowserCerotidPage1() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", ".\\libs2\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.navigate().to("http://www.cerotid.com");

		driver.manage().window().maximize();

		TimeUnit.SECONDS.sleep(4);

		driver.navigate().refresh();

		String titleName = "Cerotid";
		if (driver.getTitle().contains(titleName)) {

			System.out.println(driver.getTitle() + ".......................................was launched");

		} else {
			System.out.println("Expected title" + titleName + "was not seen");
			System.out.println("Test Fail-------Ending Test");

			driver.quit();
			driver.close();
		}
	}

	public static void fillform() {

		Select chooseCourse = new Select(CerotidWebsitePageElements.selectCourse(driver));
		chooseCourse.selectByVisibleText("QA Automation");

		Select chooseSession = new Select(CerotidWebsitePageElements.chooseSession(driver));
		chooseSession.selectByVisibleText("Upcoming Session");

		CerotidWebsitePageElements.enterName(driver).sendKeys("Sammie");

		CerotidWebsitePageElements.inputAddress(driver).sendKeys("113 Beltline Rd");

		CerotidWebsitePageElements.inputCity(driver).sendKeys("Irving");
		
		CerotidWebsitePageElements.selectState(driver).sendKeys("Tx");

		CerotidWebsitePageElements.inputzip(driver).sendKeys("75038");

		CerotidWebsitePageElements.inputemail(driver).sendKeys("princessammi07@gmail.com");
		
		CerotidWebsitePageElements.inputphone(driver).sendKeys("abcdefghij");

		CerotidWebsitePageElements.selectVisaStatus(driver).sendKeys("Green Card");

		CerotidWebsitePageElements.selectmediaSource(driver).sendKeys("Frieds/Family");
		
		CerotidWebsitePageElements.inputrelocate(driver).sendKeys("Yes");
		
		CerotidWebsitePageElements.inputeducation(driver).sendKeys("Bachelors Running");

	
	
		
		

	}

}
