package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CerotidWebsitePageElements {
//Global Variable
	static WebElement element;

	// Course Element;
	public static WebElement selectCourse(WebDriver driver) {

		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;

	}

	// Session Element
	public static WebElement chooseSession(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id= 'sessionType']"));
		return element;

	}
//Full name Element
	public static WebElement enterName(WebDriver driver){
		
	
	
	element =  driver.findElement(By.xpath("(//input[@data-validation-required-message=\"Please enter your name.\"])[1]"));
	
	return element;}
	
	
	public static WebElement inputAddress(WebDriver driver){
		element = driver.findElement(By.xpath("//input[@id='address']"));
				return element;}
	
	public static WebElement inputCity(WebDriver driver){
		element = driver.findElement(By.xpath("//input[@id='city']"));
				return element;}
	public static WebElement selectState(WebDriver driver){
		element = driver.findElement(By.xpath("//select[@id='state']"));
				return element;}
	public static WebElement inputzip(WebDriver driver){
		element = driver.findElement(By.xpath("//input[@id='zip']"));
				return element;}
	public static WebElement inputemail(WebDriver driver){
		element = driver.findElement(By.xpath("//input[@id='email']"));
				return element;}
	public static WebElement inputphone(WebDriver driver){
		element = driver.findElement(By.xpath("//input[@id='phone']"));
				return element;}
	public static WebElement selectVisaStatus(WebDriver driver){
		element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
				return element;}
	public static WebElement selectmediaSource(WebDriver driver){
		element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
				return element;}
	public static WebElement inputrelocate(WebDriver driver){
		element = driver.findElement(By.xpath("//input[@id='relocate']"));
				return element;}
	public static WebElement inputeducation(WebDriver driver){
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
				return element;}
	
	}
