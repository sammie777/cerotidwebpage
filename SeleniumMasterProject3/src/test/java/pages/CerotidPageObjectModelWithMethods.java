package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectModelWithMethods {

	WebDriver driver = null;

	public CerotidPageObjectModelWithMethods(WebDriver driver) {
		this.driver = driver;

	}

	// Creating By obj and finding elements by xpath
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id= 'sessionType']");
    By inputName = By.xpath("(//input[@data-validation-required-message=\"Please enter your name.\"])[1]");
	// Choose Course
	public void selectCourse(String courseName) {
		// creating new webelement obj
		WebElement element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(courseName);
	}
	public void selectSession(String sessionName) {
		//Creating new webElement object
		WebElement element = driver.findElement(selectSession);
		Select selectSession = new Select(element);
		selectSession.selectByVisibleText(sessionName);
	}
		public void inputName(String enterName) {
			//Creating new webElement object
			WebElement element = driver.findElement(inputName);
			Select inputName = new Select(element);
			inputName.selectByVisibleText(enterName);
			
			
			
			

		
		
		
		
		
	}

}
