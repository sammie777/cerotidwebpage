package DesiredCapabilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Incognito {

	
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", ".\\libs2\\chromedriver.exe");
		
		//Incognito
	ChromeOptions options = new ChromOptions();
		options.addArguments("--incognito");
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		capabilities.setCapability(ChromeOptions.CAPABILITY,options);
		
		WebDriver driver = new ChromeDriver(options);
		 driver.get("https://www.guru99.com/smoke - testing.html#7");
		 
		 
	}
}
