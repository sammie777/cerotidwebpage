package DesiredCapabilities;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ADBlocker {
	

	public static void main(String[] args) {
		
	
	System.setProperty("webdriver.chrome.driver", ".\\libs2\\chromedriver.exe");
		
	ChromeOptions options = new ChromeOptions();
	 options.addExtensions(new File(".\\libs2\\extension_4_6_0_0.crx"));
	 DesiredCapabilities capabilities = new DesiredCapabilities();
	 capabilities.setCapability(ChromeOptions.CAPABILITY, options);
	options.merge(capabilities);
	
	WebDriver driver = new ChromeDriver(options);
	driver.get("https://www.msn.com/en-us/news/us/death-toll-rises-to-25-across-four-counties-after-tornadoes-hit-nashville-middle-tennessee");
	
	
	}
	}
